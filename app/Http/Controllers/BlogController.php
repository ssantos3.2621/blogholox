<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $posts = Post::orderBy('id','DESC')->where('is_published','1')->paginate(5);

        $categoria = Category::all();

        return view('frontend.index', compact('posts','categoria'));
    }


    public function post(Post $post)
    {
        $post = $post->load(['comments.user', 'tags', 'user', 'category']);

        return view('frontend.post', compact('post'));
    }

    public function comment(Request $request, Post $post)
    {
        $this->validate($request, ['body' => 'required']);

        $post->comments()->create([
            'body' => $request->body
        ]);
        flash()->overlay('Comment successfully created');

        return redirect("/posts/{$post->id}");
    }

    public function categoria($categoria){
        $cate = Category::where('name', $categoria)->pluck('id')->first();

        $posts = Post::where('category_id', $cate)
            ->orderBy('id', 'DESC')->where('is_published','1')->paginate(5);

        $categoria = Category::all();

        return view('frontend.index', compact('posts','categoria'));
    }
}
