@extends('layouts.app')

@section('content')
 <section class="page-title bg-overlay-black-0" style="background-image: url({{ $post->file }});background-position: center;
  background-repeat: no-repeat;
  background-size: cover;margin-top: 150px;height: 100vh;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          <div class="page-title-name">
            </div>
         </div>
         </div>
      </div>
    </section>

    <div class="container">
        <div class="row" style="min-height: 100vh">

            <div class="col-md-12">
                <div class="panel-default">
                    <div class="panel-heading pt-5 border border-0" style="background-color: #f8f8f8!important;">
                        <h1 style="color: #572983;">{{ $post->title }}</h1>
                        <h3 class="py-3"><img src="{{asset('images/icono_blog1.png')}}" width="40px" class="pr-3">{{ $post->created_at->format('d/m/Y') }}</h3>
                        <h3><img src="{{asset('images/icono_blog-02.png')}}" width="40px" class="pr-3">{{ $post->category->name }}</h3>
                        @foreach($post->tags as $tag)
                          <h3 class="pb-5"><img src="{{asset('images/icono_blog3.png')}}" width="40px" class="pr-3">{{ $tag->name }}</h3>
                          @endforeach
                    </div>

                    <div class="panel-body pt-5">
                        <h4>{{ $post->body }}</h4>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
