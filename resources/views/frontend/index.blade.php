@extends('layouts.app')

@section('content')

    <section class="page-title bg-overlay-black-0" style="background-image: url(images/blog_cover2.jpg);margin-top: 150px;background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          <div class="page-title-name">
            </div>
         </div>
         </div>
      </div>
    </section>

    <div class="container-fluid py-5">
        <div class="row">

            <div class="col-md-2 pl-5" style="padding-top: 60px;">
               <h1 style="color: #572983;">Categorias</h1>
                @foreach ($categoria as $categorias)
                <h3 class="pl-5"><a a href="{{ route('/',$categorias->name) }}">
                  -{{ $categorias->name }}
                </a></h3>
                 @endforeach
            </div>

            <div class="col-md-10 pr-5">
                @forelse ($posts as $post)
                    <div class="panel panel-default col-md-12 py-5">
                        <div class="col-md-4">
                            <img src="{{$post->file}}" width="100%">
                        </div>
                        <div class="panel-body col-md-8 pr-5">
                            <h3 style="color: #572983;">{{ $post->title }}</h3>
                            <h6><img src="{{asset('images/icono_blog1.png')}}" width="30px" class="pr-3">{{ $post->created_at->format("d/m/y") }}</h6>
                            <h6><img src="{{asset('images/icono_blog-02.png')}}" width="30px" class="pr-3">{{ $post->category->name }}</h6>
                            <p style="color:black!important;">{{ str_limit($post->body, 200) }}</p>
                            <h4 class="text-right pr-5" style="color: #572983;text-decoration: underline;font-style: italic;">
                                <a href="{{ url("/posts/{$post->id}") }}">Leer más</a>
                            </h4>
                        </div>
                    </div>
                @empty
                    <div class="panel panel-default">
                        <div class="panel-heading">Not Found!!</div>

                        <div class="panel-body">
                            <p>Sorry! No post found.</p>
                        </div>
                    </div>
                @endforelse
                <div class="container py-3 d-flex justify-content-center">
                    {!! $posts->links() !!}
                    </div>
                </div>
        </div>
    </div>

@endsection
