<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="blog olox" />
<meta name="description" content="olox - terapias" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Login Holox</title>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Favicon -->
<link rel="shortcut icon" href="" />
<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link href="https://fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">
<script src="https://kit.fontawesome.com/5b24347517.js" crossorigin="anonymous"></script>
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins-css.css') }}" />
<!-- Typography -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/typography.css') }}" />
<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/shortcodes/shortcodes.css') }}" />
<!-- Style -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
<!-- plumber -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/plumber.css') }}" />
<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" />
<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/skins/skin-yellow.css') }}" data-style="styles"/>
<!-- Scripts -->
<script>
        window.olox = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>
<style>
::placeholder {
  color: white!important;
  opacity: 1; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white!important;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white!important;
}
</style>
</head>

<body style="background-image: url({{asset('images/fondo_login.jpg')}});background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
<div id="app">

{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


 --}}
<!--=================================
 login-->

<section class="section-transparent page-section-pb" style="height: 100vh!important;display: flex;
   justify-content: center;
   align-items: center;padding-bottom: 0px">
  <div class="container">
     <div class="row justify-content-center">
       <div class="col-lg-5 col-md-7">
       <div class="logo text-center mb-30 mt-30">

         </div>
        <div class="login-bg clearfix">
           <div class="login-title text-center pt-3" style="background: transparent;">
             <a href="{{ url('/') }}"><img class="img-fluid" width="50%" id="logo_img" src="{{asset('images/logo-morado.png')}}" alt="logo"></a>
            </div>
             <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="py-3 d-flex justify-content-center{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-10 mx-auto">
                                <input style="background-color: #572983" id="email" type="email" class="form-control py-2 px-3" name="email" placeholder="USUARIO" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="pt-5 pb-5 d-flex justify-content-center{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-10">
                                <input style="background-color: #572983;" id="password" type="password" class="form-control py-2 px-3" placeholder="CONTRASEÑA" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group pb-5" style="padding-top: 70px;">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn" style="padding: 0.2rem .75rem!important;width:20%;border-radius:6px;background-color:#572983;color:white;font-size: 12px">
                                    ENTRAR
                                </button>
                            </div>
                        </div>
                    </form>
           </div>
        </div>
      </div>
  </div>
</section>

<!--=================================
 login-->













</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>
<!--=================================
 jquery -->

<!-- jquery -->
<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>

<!-- plugins-jquery -->
<script src="{{ asset('js/plugins-jquery.js') }}"></script>

<!-- plugin_path -->
<script>var plugin_path = '{{ asset('/js/') }}';</script>

<!-- Google recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- custom -->
<script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>
