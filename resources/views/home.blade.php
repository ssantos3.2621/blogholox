@extends('layouts.app')

@section('content')
    <div class="container separacion">
        <div class="row pt-5" style="height: 100vh">

            <div class="col-md-4">
                <div class="col-md-12 card">
                    <div class="panel-heading" style="border: none!important;border-bottom: 1px solid black!important;">Publicaciones</div>
                    <div class="panel-body">
                        <h1>{{ $posts }}</h1>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="col-md-12 card">
                    <div class="panel-heading" style="border: none!important;border-bottom: 1px solid black!important;">Etiquetas</div>
                    <div class="panel-body">
                        <h1>{{ $tags }}</h1>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="col-md-12 card">
                    <div class="panel-heading" style="border: none!important;border-bottom: 1px solid black!important;">Categorias</div>
                    <div class="panel-body">
                        <h1>{{ $categories }}</h1>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
