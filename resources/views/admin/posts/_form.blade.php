<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    {!! Form::label('title', 'Titulo', ['class' => 'col-md-2 control-label colorm']) !!}

    <div class="col-md-8">
        {!! Form::text('title', null, ['class' => 'form-control p-2', 'required', 'autofocus']) !!}

        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    </div>
</div>

<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
    {!! Form::label('body', 'Texto', ['class' => 'col-md-2 control-label colorm']) !!}

    <div class="col-md-8">
        {!! Form::textarea('body', null, ['class' => 'form-control p-2', 'required']) !!}

        <span class="help-block">
            <strong>{{ $errors->first('body') }}</strong>
        </span>
    </div>
</div>

<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'Categoría', ['class' => 'col-md-2 control-label colorm']) !!}

    <div class="col-md-8">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control p-2', 'required']) !!}

        <span class="help-block">
            <strong>{{ $errors->first('category_id') }}</strong>
        </span>
    </div>
</div>

@php
    if(isset($post)) {
        $tag = $post->tags->pluck('name')->all();
    } else {
        $tag = null;
    }
@endphp

<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
    {!! Form::label('tags', 'Etiqueta', ['class' => 'col-md-2 control-label colorm']) !!}

    <div class="col-md-8">
        {!! Form::select('tags[]', $tags, $tag, ['class' => 'form-control p-2 select2-tags', 'required', 'multiple']) !!}

        <span class="help-block">
            <strong>{{ $errors->first('tags') }}</strong>
        </span>
    </div>
</div>
<div class="form-group">
    {{ Form::label('file', 'Subir Imagen',['class' => 'col-md-2 control-label colorm'])}}
    <div class="col-md-8">
    {!! Form::file('file') !!}
    </div>
</div>