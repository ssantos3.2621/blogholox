@extends('layouts.app')

@section('content')
    <div class="container separacion">
        <div class="row" style="height: 100vh">
            <div class="col-md-12 pt-5">
            <div class="col-md-12 card" style="border-radius:35px;box-shadow: 0 .5rem 1rem rgba(0,0,0,.25)!important;">
                    <div class="col-md-12 panel-heading px-5" style="margin-top: 30px;border: none;">
                       <div class="col-xs-12 col-md-6"><h1 id="xs-fonts" style="color:#572983;font-size: 50px">Publicaciones</h1></div>
                        <div class="col-xs-12 col-md-6 xs-alin">
                            <form action="{{route('eliminar')}}" method="post">
                            {{ csrf_field() }}
                             <input type="hidden" name="_method" value="delete">
                            <button style="border:none;background-color:transparent;" type="submit" name="borrar" data-confirm="Esta seguro?" class="pull-right mr-3"><img src="{{asset('images/eliminar.png')}}" width="40px"></button>
                            <a onclick="editarpub()" class="pull-right mr-3"><img src="{{asset('images/editar.png')}}" width="40px"></a>
                            <a href="{{ url('admin/posts/create') }}" class="pull-right mr-4"><img src="{{asset('images/crear.png')}}" width="40px"></a>
                        </div>
                    </div>

                    <div class="panel-body col-md-12">
                        <table class="table col-md-12">
                            <thead>
                                <tr>
                                    <th id="xs-none">Fecha</th>
                                    <th>Titulo</th>
                                    <th id="xs-none">Creado por</th>
                                    <th id="xs-none">Categoría</th>
                                    <th id="xs-none">Publicado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($posts as $post)
                                    <tr>
                                        <td id="xs-none">{{ $post->created_at->format("d/m/y") }}</td>
                                        <td>{{ $post->title }}</td>
                                        <td id="xs-none">{{ $post->user->name }}</td>
                                        <td id="xs-none">{{ $post->category->name }}</td>
                                        <td id="xs-none">{{ $post->published }}</td>
                                        <td>
                                            @if (Auth::user()->is_admin)
                                                @php
                                                    if($post->published == 'Yes') {
                                                        $label = 'Borrador';
                                                    } else {
                                                        $label = 'Publicar';
                                                    }
                                                @endphp
                                                <a href="{{ url("/admin/posts/{$post->id}/publish") }}" data-method="PUT" data-token="{{ csrf_token() }}" data-confirm="Esta seguro?" class="btn btn-xs btn-warning">{{ $label }}</a>
                                            @endif
                                            <input onclick="idpublicacion({{$post->id}})" type="radio" id="action" name="eliminar[]" value="{{$post->id}}">
                                            {{-- <a href="{{ url("/admin/posts/{$post->id}/edit") }}" class="btn btn-xs btn-info">Editar</a>
                                            <a href="{{ url("/admin/posts/{$post->id}") }}" data-method="DELETE" data-token="{{ csrf_token() }}" data-confirm="Esta seguro?" class="btn btn-xs btn-danger">Eliminar</a> --}}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No post available.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="container py-3 d-flex justify-content-center">
                    {!! $posts->links() !!}
                    </div>
                </div>
            </form>
            </div>

        </div>
    </div>
    <script type="text/javascript">

        var idpost= "";
        function idpublicacion(id){
            idpost = id;
        }
        function editarpub(){
            window.location.href="http://holox.guru/blog/public/admin/posts/"+idpost+"/edit";
        }
    </script>
@endsection

