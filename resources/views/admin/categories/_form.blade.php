<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

    {!! Form::label('name', 'Nombre de la categoria', ['class' => 'col-md-3 control-label colorm']) !!}

    <div class="col-md-7">
        {!! Form::text('name', null, ['class' => 'form-control p-2', 'required', 'autofocus']) !!}

        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    </div>
</div>
