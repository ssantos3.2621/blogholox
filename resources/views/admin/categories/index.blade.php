@extends('layouts.app')

@section('content')
    <div class="container separacion">
        <div class="row" style="height: 80vh">

            <div class="col-md-12 pt-5">
            <div class="col-md-12 card" style="border-radius:35px;box-shadow: 0 .5rem 1rem rgba(0,0,0,.25)!important;">
                    <div class="col-md-12 panel-heading" style="margin-top: 40px;border: none;">
                        <div class="col-xs-12 col-md-6"><h1 style="color:#572983;font-size: 50px">Categorias</h1></div>
                        <div class="col-xs-12 col-md-6 xs-alin">
                             <form action="{{route('eliminarc')}}" method="post">
                            {{ csrf_field() }}
                             <input type="hidden" name="_method" value="delete">
                            <button style="border:none;background-color:transparent;" type="submit" name="borrar" data-confirm="Esta seguro?" class="pull-right mr-3"><img src="{{asset('images/eliminar.png')}}" width="40px"></button>
                            <a onclick="editarpub()" class="pull-right mr-3"><img src="{{asset('images/editar.png')}}" width="40px"></a>
                            <a href="{{ url('admin/categories/create') }}" class="pull-right mr-3"><img src="{{asset('images/crear.png')}}" width="40px"></a>
                        </div>
                    </div>

                    <div class="col-md-12 panel-body">
                        <table class="col-md-12 table">
                            <thead>
                                <tr>
                                    <th>Nombre de la categoria</th>
                                    <th>No. de entradas</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($categories as $category)
                                    <tr>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->posts_count }}</td>
                                        <td>
                                            <input onclick="idpublicacion({{ $category->id }})" type="radio" id="action" name="eliminar[]" value="{{ $category->id }}">
                                            {{-- <a href="{{ url("/admin/categories/{$category->id}/edit") }}" class="btn btn-xs btn-info">Editar</a>
                                            <a href="{{ url("/admin/categories/{$category->id}") }}" data-method="DELETE" data-token="{{ csrf_token() }}" data-confirm="Estas seguro?" class="btn btn-xs btn-danger">Eliminar</a> --}}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">No hay categorías</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        {!! $categories->links() !!}

                    </div>
                </div>
            </div>

        </div>
    </div>

     <script type="text/javascript">

        var idpost= "";
        function idpublicacion(id){
            idpost = id;
        }
        function editarpub(){
            window.location.href="http://holox.guru/blog/public/admin/categories/"+idpost+"/edit";
        }
    </script>
@endsection
