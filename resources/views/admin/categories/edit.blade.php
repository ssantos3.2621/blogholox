@extends('layouts.app')

@section('content')
    <div class="container separacion">
        <div class="row" style="height: 80vh">

            <div class="col-md-12 py-5 d-flex justify-content-center">
            <div class="col-md-10 card" style="height: fit-content;border-radius:35px;box-shadow: 0 .5rem 1rem rgba(0,0,0,.25)!important;">
                    <div class="panel-heading text-center" style="margin-top: 40px;border: none;">
                        <h1 style="color:#572983;font-size: 50px">Editar Categoria</h1>
                    </div>

                    <div class="panel-body">
                        {!! Form::model($category, ['method' => 'PUT', 'url' => "/admin/categories/{$category->id}", 'class' => 'form-horizontal', 'role' => 'form']) !!}

                            @include('admin.categories._form')

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        <img src="{{asset('images/guardar.png')}}" width="10px" class="mr-3 mb-1">GUARDAR
                                    </button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
