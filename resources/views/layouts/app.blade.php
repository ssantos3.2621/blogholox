<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="blog olox" />
<meta name="description" content="olox - terapias" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Holox Blog</title>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('images/logo-blanco.png')}}" />
<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link href="https://fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">
<script src="https://kit.fontawesome.com/5b24347517.js" crossorigin="anonymous"></script>
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins-css.css') }}" />
<!-- Typography -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/typography.css') }}" />
<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/shortcodes/shortcodes.css') }}" />
<!-- Style -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
<!-- plumber -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/plumber.css') }}" />
<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" />
<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
<!-- Scripts -->
<script>
        window.olox = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>
</head>

<body>
  @if (Auth::guest())
<div id="app" style="background-color: #f8f8f8">
  @else
<div id="app" style="background-image: url({{asset('images/fondo_blog2.jpg')}});background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
 @endif

<div id="pre-loader">
    <img src="{{ asset('images/loader-03.svg') }}" alt="">
</div>

  @if (Auth::guest())
<!--================================= header -->
<header id="header" class="header default" style="background-color: white;">
  <div class="menu" id="onepagenav">
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items menuB">
     <div class="container container2">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <!-- menu logo -->
        <ul class="menu-logo padding0">
            <li>
                <a href="{{ url('/') }}"><img id="imgh" class="img-fluid" src="images/logo_header.png"> </a>
                <a id="wts" style="display: none;padding-right:15%" class="float-right" target="_blank" href="tel:2227590940">
              <img id="logo_img" src="{{asset('images/boton_llamada.png')}}" style="margin-top:30%;height: 35px!important" alt="logo">
            </a>
            <a id="tel" style="display: none;" class="float-right" target="_blank" href="https://wa.me/522227590940?text=Me%20gustaría%20solicitar%20información">
              <img id="logo_img" src="{{asset('images/boton_whats.png')}}" style="margin-top:30%;height: 35px!important" alt="logo">
            </a>
            <a id="tel" style="display: none;" class="float-right"  href="https://m.me/holoxguru">
              <img id="logo_img" src="{{asset('images/boton_messenger.png')}}" style="margin-top:30%;height: 35px!important" alt="logo">
            </a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="menu-links">
            <li><a class="textoMT aMenu" href="#nosotros">NOSOTROS</a></li>
            <li><a class="textoMT aMenu" href="#servicios">SERVICIOS</a></li>
            <li><a class="textoMT aMenu" href="#contacto">CONTACTO</a></li>
            <li><a class="textoMT aMenu" href="http://holox.guru/blog/public/">LOGIN</a></li>
            <li class="menuI"><a target="_blank" href="https://wa.me/522227590940?text=Me%20gustaría%20solicitar%20información"><span><img src="{{url('images/boton_whats.png')}}" width="45" style="margin: 15px 0px" alt="animatiomx"></span></a>
            </li>
            <li class="menuI"><a href="https://m.me/holoxguru"><span><img src="{{url('images/boton_messenger.png')}}" width="45" alt="animatiomx"></span></a></li>
            <li class="menuI"><a target="_blank" href="tel:2227590940"><span><img src="{{url('images/boton_llamada.png')}}" width="45" alt="animatiomx"></span></a>
            </li>
          </ul>
        </div>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>

<!--=================================
 header -->
             @else
          <!--=================================
 header -->

<header id="header" class="header default desaparecer" style="background-color: #572983;">
  <div class="menu" id="onepagenav">
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container-fluid">
      <div class="row">
       <div class="col-lg-12 col-md-12">

        <div class="col-md-5" style="padding-top: 55px;font-size: 30px;font-weight: 400">
         <ul class="d-flex justify-content-around">
          <li><a style="color: white!important;" href="{{ url('admin/posts') }}">Publicaciones</a></li>
                <li><a style="color: white!important;" href="{{ url('admin/categories') }}">Categorias</a></li>
          </ul>
        </div>
        <!-- menu logo -->
        <ul class="menu-logo col-md-2 d-flex justify-content-center">
            <li>
            <a class="navbar-brand" href=""><img class="img-fluid" src="{{asset('images/logo-blanco.png')}}" alt=""></a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="col-md-5" style="padding-top: 55px;font-size: 30px;font-weight: 400">
         <ul class="d-flex justify-content-around">
                <li><a style="color: white!important;" href="{{ url('admin/tags') }}">Etiquetas</a></li>
                <li><a style="color: white!important;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesion</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                    </form>
                </li>
              </ul>
        </div>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>
<!-- header  responsivo blog-->
<header id="header" class="header default aparecer" style="visibility: hidden;background-color: #572983!important;">
  <div class="menu" id="onepagenav">
  <!-- menu start -->
   <nav id="menu" class="mega-menu navbar-dark">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container-fluid">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <!-- menu logo -->
        <ul class="menu-logo">
            <li>
              <button style="z-index:999;" class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}"><img class="img-fluid" src="{{asset('images/logo-blanco.png')}}" alt=""></a>
            <a id="wts" style="display: none;margin-right:4%" class="float-right" href="tel:522222475976">
              <img id="logo_img" src="{{asset('images/boton_llamada1.png')}}" style="height: 35px!important" alt="logo">
            </a>
            <a id="tel" style="display: none;padding-right:4%" class="float-right" href="https://wa.me/2223125949?text=Me%20gustaría%20pedir%20una%20cotización">
              <img id="logo_img" src="{{asset('images/boton_whats.png')}}" style="height: 35px!important" alt="logo">
            </a>
            <a id="tel" style="display: none;padding-right:4%" class="float-right" href="">
              <img id="logo_img" src="{{asset('images/boton_messenger.png')}}" style="height: 35px!important" alt="logo">
            </a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="collapse navbar-collapse menu-links" id="navbarText" style="background-color:#f8f8f8f8;">
                  <li><a style="color:black!important;" href="{{ url('admin/posts') }}">Publicaciones</a></li>
                  <li><a style="color:black!important;" href="{{ url('admin/categories') }}">Categorias</a></li>
                  <li><a style="color:black!important;" href="{{ url('admin/tags') }}">Etiquetas</a></li>
                  <li><a style="color:black!important;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesion</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                  </li>
          </ul>
        </div>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>
            @endif

    <div class="container">
        <div class="row">
            @include('flash::message')
        </div>
    </div>


        @yield('content')

@if (Auth::guest())

<!--================================= footer -->
<footer class="footer footer-topbar" style="background-image:url({{asset('images/f2.jpg')}});background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;">
<div class="container">
    <div class="row pt-5 pb-0">
        <div class="col-lg-2 pl-5" id="imgfooter">
          <img id="xs-small" class="img-fluid" src="{{asset('images/logo-morado-oscuro.png')}}" alt="" style="width:140px;height:140px;">
        </div>
      <div class="col-lg-3" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-20">
          <div class="row d-flex justify-content-center">
            <a href="#" class="d-flex justify-content-center">
              <img id="xs-small2" class="img-fluid" src="{{asset('images/footer_ubicacion.png')}}" alt="" style="width:50px;height:50px">
            </a>
          </div>
          <div class="row d-flex justify-content-center text-center" style="color: #572983;font-weight: bolder;">
           <span  id="xs-font">6 Pte. 504 Centro San<br> Pedro Cholula Pue.</span>
          </div>
        </div>
      </div>
      <div class="col-lg-2" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-20">
          <div class="row d-flex justify-content-center">
            <a href="#" class="d-flex justify-content-center">
              <img id="xs-small2" class="img-fluid" src="{{asset('images/footer_llamada.png')}}" alt="" style="width:50px;height:50px">
            </a>
          </div>
          <div class="row d-flex justify-content-center" style="color: #572983;font-weight: bolder;">
                <span  id="xs-font2">22 27 59 09 40</span>
          </div>
        </div>
      </div>
      <div class="col-lg-3" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-20">
            <div id="xs-small3" class="row d-flex justify-content-center">
              <a href="#" class="d-flex justify-content-center">
                <img id="xs-small2" class="img-fluid" src="{{asset('images/footer_correo.png')}}" alt="" style="width:50px;height:50px;">
              </a>
            </div>
            <div class="row d-flex justify-content-center" style="color: #572983;font-weight: bolder;">
                <span  id="xs-font2">info@holox.guru</span>
            </div>
        </div>
      </div>
      <div class="col-lg-2" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-40">
             <ul class="clearfix">
              <li><a href="#"><img class="img-fluid" src="{{asset('images/footer_redes1.png')}}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{asset('images/footer_redes2.png')}}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{asset('images/footer_redes3.png')}}" alt=""></a></li>
             </ul>
        </div>
      </div>
    </div>
    <div id="separa2" class="row float-right pb-5"><span style="color: black;">2020 Todos los derechos reservados ANIMATIOMX</span></div>
</div>
</footer>
 @else

 @endif



</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>
<!--=================================
 jquery -->

<!-- jquery -->
<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>

<!-- plugins-jquery -->
<script src="{{ asset('js/plugins-jquery.js') }}"></script>

<!-- plugin_path -->
<script>var plugin_path = '{{ asset('/js/') }}';</script>

<!-- Google recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- custom -->
<script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>
